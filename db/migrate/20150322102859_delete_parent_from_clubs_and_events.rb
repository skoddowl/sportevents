class DeleteParentFromClubsAndEvents < ActiveRecord::Migration
  def change
    remove_column :club_types, :parent_id
    remove_column :event_types, :parent_id
  end
end
