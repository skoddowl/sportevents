class ChangePriceInClubsAndEvents < ActiveRecord::Migration
  def change
    change_column :clubs, :price, :string
  end
end
