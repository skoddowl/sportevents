class RenameDeletedAuthorAndDeletedRecipient < ActiveRecord::Migration
  def change
    rename_column :messages, :deleted_author, :deleted_by_author
    rename_column :messages, :deleted_recipient, :deleted_by_recipient
  end
end
