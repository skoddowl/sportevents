class AddPositionToClubtypeAndEventtype < ActiveRecord::Migration
  def change
    add_column :club_types, :position, :integer
    add_column :event_types, :position, :integer
  end
end
