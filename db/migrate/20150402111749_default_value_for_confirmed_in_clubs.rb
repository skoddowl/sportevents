class DefaultValueForConfirmedInClubs < ActiveRecord::Migration
  def change
    change_column :clubs, :confirmed, :boolean, default: false
  end
end
