class UserMailer < ApplicationMailer
  default from: 'Sportevents'

  def email_confirmation(user, email, code)
    @user, @code, @email = user, code, email
    subject = "Підтвердження електронної адреси на Sportevents CK"
    mail(to: email, subject: subject)
  end

  def news_feed(email, unread_events)
    @unread_events = unread_events
  	subject = "Оновлення на Sportevents"
  	mail(to: email, subject: subject)
  end
end
