class ApplicationMailer < ActionMailer::Base
  default from: "Sportevents"
  layout 'mailer'
end
