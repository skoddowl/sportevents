function showMap(){
    var map = L.map('map').setView([49.437820, 32.058959], 13);
    L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: mapTitle,
        id: 'examples.map-i875mjb7'
    }).addTo(map);

    var GreenIcon = L.Icon.Default.extend({
            options: {
                    iconUrl: '/assets/marker.png' 
            }
         });

    var greenIcon = new GreenIcon();

    for (var i = 0; i < markersArray.length; i++) {
            L.marker(markersArray[i].latlng, {icon: greenIcon}).addTo(map).bindPopup(markersArray[i].popup);
        }
    }

function showLocation(){
    var map = L.map('map').setView([49.437820, 32.058959], 13);

    L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: name,
        id: 'examples.map-i875mjb7'
    }).addTo(map);

    var GreenIcon = L.Icon.Default.extend({
            options: {
                    iconUrl: '/assets/marker.png' 
            }
         });
    
    var greenIcon = new GreenIcon();

    L.marker(latlng, {icon: greenIcon}).addTo(map)
        .bindPopup("<b>" + name +"</b><br/>Категория: " + type);
    }

function addLocation() {
    var map = L.map('map').setView([49.437820, 32.058959], 13);

    L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
        maxZoom: 18,
        id: 'examples.map-i875mjb7'
    }).addTo(map);

    var marker;
    if (presence == true) {
        marker = addMarker(latlng[0], latlng[1]);
    }

    function onMapClick(e) {
        if (marker != null) {
            map.removeLayer(marker);
        }
        marker = addMarker(e.latlng.lat, e.latlng.lng);
    }

    map.on('click', onMapClick);

    function addMarker(lat, lng) {
        
        var GreenIcon = L.Icon.Default.extend({
            options: {
                    iconUrl: '/assets/marker.png' 
            }
        });

        var greenIcon = new GreenIcon();
        
        var new_marker = new L.Marker([lat, lng], { draggable:true, icon: greenIcon});
        map.addLayer(new_marker);
        $('#location').val([lat, lng]);
        return new_marker;
    }

   
}