ActiveAdmin.register Event do

  config.filters = false
  config.per_page = 10

  scope :not_started, default: true
  scope :started

  permit_params :name, :event_type_id, :description, :begins_at, :phone

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


end
