class MessagesController < ApplicationController

  before_action :set_message, only: [ :show, :destroy ]
  before_action :check_current_user, only: :new

  def inbox
    @messages = current_user.inbox(params[:page])
  end

  def sentbox
    @messages = current_user.sentbox(params[:page])
  end

  def show
    @message.mark_as_read_by(current_user)
  end

  def new
    @recipient = User.find(params[:recipient_id])
    @message = current_user.sent_messages.build
  end

  def create
    @message = current_user.sent_messages.build(message_params)
    if @message.save
      flash[:notice] = 'Ваше повідомлення було відправлене'
      redirect_to sentbox_path
    else
      render :new
    end
  end

  def destroy
    @message.delete_by(current_user)
    redirect_to :back
  end

  private

  def set_message
    @message = Message.find(params[:id])
  end

  def message_params
    params.require(:message).permit(:subject, :body, :recipient_id)
  end

  private

  def check_current_user
    if !current_user.present? || current_user.id == params[:recipient_id].to_i
      redirect_to root_path, alert: 'Ви не можете відправити повідомлення цьому користувачу'
    end
  end

end
