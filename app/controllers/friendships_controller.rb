class FriendshipsController < ApplicationController

  before_action :check_current_user

  def create
    @friendship = current_user.friendships.build(friend_id: params[:friend_id])
    if @friendship.save
      flash[:notice] = 'Користувач був доданий в друзі'
      redirect_to :back
    else
      flash[:notice] = 'Користувач не був доданий в друзі'
      redirect_to :back
    end
  end

  def destroy
    @friendship = current_user.friendships.where(friend_id: params[:friend_id])
    @friendship.destroy_all
    flash[:notice] = 'Користувач був видалений з друзів'
    redirect_to :back
  end

  private

  def check_current_user
    if !current_user.present?
      redirect_to :back, alert: 'Спочатку зареєструйтесь'
    elsif current_user.id == params[:friend_id] || current_user.friends.where(id: params[:friends_id]).any?
      redirect_to :back
    end
  end

end
