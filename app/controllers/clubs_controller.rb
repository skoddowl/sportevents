class ClubsController < ApplicationController

  include Locationable
  # helper UsersHelper

  before_action :set_club, only: [ :show, :edit, :update ]
  before_action :check_current_user, only: :new
  before_action :check_club_author, only: :edit
  before_action :set_main_controller

  def index
    if params[:search_by_type].present?
      search_hash = Club.search_by_type(params[:search_by_type])
    elsif params[:search_by_name].present?
      search_hash = Club.search_by_name(params[:search_by_name])
    else
      search_hash = Club.search_confirmed
    end
    @clubs, @message, @map_title = search_hash[:clubs].page(params[:page]), search_hash[:message], search_hash[:map_title]
    @all_clubs = search_hash[:clubs]
    @club_types = ClubType.all
  end

  def new
    @club = Club.new
  end

  def create
    @club = Club.new(club_params)
    respond_to do |format|
      if @location && @club.save
        format.html { redirect_to @club, notice: 'Секція буде додана після проходження модерації' }
      else
        flash[:alert] = 'Поставьте мітку на карті' if @location.nil?
        format.html { render :new }
      end
    end
  end

  def show
    @comments = @club.comments.ordered
    @comment = Comment.new
  end

  def edit
  end

  def update
    respond_to do |format|
      if @club.update_attributes(club_params)
        format.html { redirect_to @club, notice: 'До секції були внесені зміни' }
      else
        format.html { render :edit, alert: 'Під час редагуванння виникли помилки' }
      end
    end
  end

  private

  def set_club
    @club = Club.find(params[:id])
  end

  def club_params
    params.require(:club).permit(:name, :club_type_id, :description, :price, :phone)
      .merge(author: current_user, location: @location)
  end

  def check_current_user
    redirect_to root_path, alert: 'Спочатку зареєструйтесь' unless current_user.present?
  end

  def check_club_author
    if (@club.author != current_user) && !current_user.admin?
      redirect_to @club, alert: 'Ви не можете внести зміни до цієї секції'
    end
  end

  def set_main_controller
    session[:main_controller] = 'clubs'
  end

end
