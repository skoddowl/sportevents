class UsersController < ApplicationController

  before_action :set_user, only: [ :show, :edit, :update ]
  before_action :check_user, only: [ :edit ]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        @user.send_email_confirmation(@user.email)
        session[:user_id] = @user.id
        format.html { redirect_to root_path, notice: 'На вашу електронну адресу було відправлено лист з підтвердженням' }
      else
        format.html { redirect_to root_path, alert: 'При реєстрації виникли помилки' }
      end
    end
  end

  def show
    @user = @user
  end

  def edit
  end

  def update
    if !params[:user][:email].empty? && params[:user][:email] != @user.email
      @user.send_email_confirmation(params[:user][:email])
      notice = 'Профіль був змінений. На вашу електронну адресу було відправлено лист з підтвердженням'
    else
      notice = 'Профіль був змінений.'
    end
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: notice }
      else
        format.html { render :edit }
      end
    end
  end

  def send_email_confirmation
    @user = User.find(params[:id])
    @user.send_email_confirmation(@user.email)
    redirect_to @user, 
      notice: 'На вашу електронну адресу було відправлено лист з підтвердженням'
  end

  def confirm_email
    @confirmation = Confirmation.where(email: params[:email]).last
    @user = User.find(@confirmation.user_id)
    if @user.confirm_email(@confirmation, params[:code])
      redirect_to @user, notice: 'Адресу електронної пошти було підтверджено.'
    else
      redirect_to @user, notice: 'Помилка.'
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def check_user
    redirect_to @user, alert: 'У вас немає прав на внесення змін до цього профіля' unless @user == current_user
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :about, :phone, :skype, :avatar)
  end

end
