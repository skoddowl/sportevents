class EventsController < ApplicationController

  include Locationable
  # helper UsersHelper

  before_action :set_event, only: [ :show, :edit, :update ]
  before_action :check_current_user, only: :new
  before_action :check_event_author, only: :edit
  before_action :set_main_controller

  def index
    if params[:search_by_type].present?
      s_hash = Event.search_by_type(params[:search_by_type])
    elsif params[:search_by_name].present?
      s_hash = Event.search_by_name(params[:search_by_name])
    else
      s_hash = Event.search_not_started
    end
    @events, @message, @map_title = s_hash[:events].page(params[:page]), s_hash[:message], s_hash[:map_title]
    @all_events = s_hash[:events]
    @event_types = EventType.all
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    respond_to do |format|
      if @location && @event.save
        format.html { redirect_to @event, notice: 'Подія була додана' }
      else
        flash[:alert] = 'Поставьте мітку на карті' if @location.nil?
        format.html { render :new, alert: 'Під час створення події виникли помилки' }
      end
    end
  end

  def show
    @comments = @event.comments.ordered
    @comment = Comment.new
  end

  def edit
  end

  def update
    respond_to do |format|
      if @event.update_attributes(event_params)
        format.html { redirect_to @event, notice: 'До секції були внесені зміни' }
      else
        format.html { render :edit, alert: 'Під час редагуванння виникли помилки' }
      end
    end
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:name, :event_type_id, :description, :begins_at, :phone)
      .merge(author: current_user, location: @location)
  end

  def check_current_user
    redirect_to root_path, alert: 'Спочатку зареєструйтесь' unless current_user.present?
  end

  def check_event_author
    if (@event.author != current_user) && !current_user.admin?
      redirect_to @event, alert: 'Ви не можете внести зміни до цієї події'
    end
  end

  def set_main_controller
    session[:main_controller] = 'events'
  end

end