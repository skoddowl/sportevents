class SessionsController < ApplicationController

  def login_with_email
    @user = User.authenticate(params[:email], params[:password])
    if @user
      create_session(@user)
    else
      redirect_to root_path, alert: 'Невдалий вхід'
    end
  end

  def login_with_oauth
    auth = request.env['omniauth.auth']
    @user = User.from_omniauth(auth)
    create_session(@user)
  end

  def destroy
    reset_session
    redirect_to root_path
  end

  private

  def create_session(user)
    session[:user_id] = user.id
    notice = if user.email_confirmed?
               'Ви ввійшли в систему'
             elsif user.email.present?
               'Ви ввійшли в систему. Рекомендуємо підтвердити вашу електронну адресу'
             else
               'Ви ввійшли в систему. Рекомендуємо вказати вашу електронну адресу'
             end
    redirect_to root_path, notice: notice
  end
end
