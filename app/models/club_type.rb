# == Schema Information
#
# Table name: club_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ClubType < ActiveRecord::Base

  has_many :clubs

  validates :name, presence: true

  default_scope { order('position ASC') }

end
