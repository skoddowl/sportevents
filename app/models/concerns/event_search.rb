module EventSearch
  extend ActiveSupport::Concern

  module ClassMethods

    def search_by_type(type)
      events = Event.where(type: type).not_started.newest
      category = EventType.find(type)
      if events.any?
        message = "Результати пошуку подій в категорії \"#{category.name}\""
      else
        message = "Поки що немає секцій в категорії \"#{category.name}\""
      end
      map_title = "Секції в категорії \"#{category.name}\""
      { events: events, message: message, map_title: map_title }
    end

    def search_by_name(name)
      events = Event.where('name LIKE ?', "%#{name}%").not_started.newest
      if events.any?
        message = "Результати пошуку подій по назві \"#{name}\""
      else
        message = "Немає подій по запиту \"#{name}\""
      end
      map_title = 'Знайдені події'
      { events: events, message: message, map_title: map_title }
    end

    def search_not_started
      events = Event.not_started.newest
      message = events.any? ? 'Всі події' : 'Найближчі події відсутні'
      { events: events, message: message, map_title: 'Всі події' }
    end

  end

end