module ClubSearch
  extend ActiveSupport::Concern

  module ClassMethods

    def search_by_type(type)
      clubs = Club.where(type: type).confirmed.newest
      category = ClubType.find(type)
      if clubs.any?
        message = "Результати пошуку секцій в категорії \"#{category.name}\""
      else
        message = "Поки що немає секцій в категорії \"#{category.name}\""
      end
      map_title = "Секції в категорії \"#{category.name}\""
      { clubs: clubs, message: message, map_title: map_title }
    end

    def search_by_name(name)
      clubs = Club.where('name LIKE ?', "%#{name}%").confirmed.newest
      if clubs.any?
        message = "Результати пошуку секцій по назві \"#{name}\""
      else
        message = "Немає секцій по запиту \"#{name}\""
      end
      map_title = 'Знайдені секції'
      { clubs: clubs, message: message, map_title: map_title }
    end

    def search_confirmed
      clubs = Club.confirmed.newest
      { clubs: clubs, message: 'Всі секції', map_title: 'Всі секції' }
    end

  end

end