# == Schema Information
#
# Table name: messages
#
#  id                   :integer          not null, primary key
#  author_id            :integer
#  recipient_id         :integer
#  subject              :string
#  body                 :text
#  deleted_by_author    :boolean          default("f")
#  deleted_by_recipient :boolean          default("f")
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  unread               :boolean          default("t")
#

class Message < ActiveRecord::Base

  paginates_per 20

  belongs_to :author, class_name: 'User'
  belongs_to :recipient, class_name: 'User'

  validates :body, presence: true, length: { maximum: 1000 }
  validates :subject, length: { within: 4..140 }, presence: true

  scope :newest, -> { order('created_at DESC') }

  def mark_as_read_by(user)
    update_attributes(unread: false) if (user.id == recipient_id)
  end

  def delete_by(user)
    if user == author
      update_attributes(deleted_by_author: true)
    elsif user == recipient
      update_attributes(deleted_by_recipient: true)
    end
    destroy if deleted_by_recipient && deleted_by_author
  end

end
