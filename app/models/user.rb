# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  provider        :string
#  uid             :string
#  name            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  email           :string
#  about           :text
#  phone           :string
#  skype           :string
#  admin           :boolean          default("f")
#  email_confirmed :boolean          default("f")
#  avatar          :string
#  password_digest :string
#  full_name       :string
#

class User < ActiveRecord::Base

  has_secure_password
  include OmniauthRegistration
  mount_uploader :avatar, AvatarUploader

  acts_as_reader

  has_many :clubs, dependent: :destroy
  has_many :events, dependent: :destroy

  has_many :friendships, dependent: :destroy
  has_many :friends, through: :friendships

  has_many :sent_messages, class_name: 'Message', foreign_key: 'author_id', dependent: :destroy
  has_many :received_messages, class_name: 'Message', foreign_key: 'recipient_id', dependent: :destroy

  has_many :confirmations

  before_save :set_password_for_omniuath_users

  validates :name, presence: true, uniqueness: true
  validates :full_name, :skype, allow_blank: true, length: { in: 3..15 }
  validates :email, uniqueness: true, allow_blank: true, email: true
  validates :about, allow_blank: true, length: { in: 0..100 }
  validates :phone, allow_blank: true, length: { in: 6..10 }

  def self.authenticate(email, password)
    user = self.find_by_email(email.downcase)
    user.authenticate(password) if user
  end

  def to_param
    "#{id}-#{name.parameterize}"
  end

  def inbox(page)
    received_messages.where(deleted_by_recipient: false).newest.page(page)
  end

  def sentbox(page)
    sent_messages.where(deleted_by_author: false).newest.page(page)
  end

  def send_email_confirmation(email)
    self.create_confirmation(email: email)
    self.update_attributes(email_confirmed: false)
  end

  def confirm_email(confirmation, code)
    if confirmation.code == code
      update_attributes(email: confirmation.email, email_confirmed: true)
      confirmations.destroy_all
      return true
    else
      return false
    end
  end

  def set_password_for_omniuath_users
    password_digest = SecureRandom.urlsafe_base64 if provider.present?
  end

end
