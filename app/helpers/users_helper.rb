module UsersHelper

  def user_can_add_to_friends?
    current_user.present? && current_user.id != @user.id && current_user.friends.where(id: @user.id).empty?
  end

  def user_can_remove_from_friends?
    current_user.present? && !current_user.friends.where(id: @user.id).empty?
  end

  def user_can_edit?(obj)
    (current_user == obj.author) || (current_user && current_user.admin?)
  end

  def user_can_send_message_to?(user)
    current_user && (current_user != user)
  end

  def show_email_alert?
    !@user.email.to_s.empty? && @user == current_user
  end
end
