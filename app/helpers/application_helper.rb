module ApplicationHelper

  def go_to
    if session[:main_controller] == 'clubs'
      link = link_to 'Перейти до подій', events_path, class: 'go-to'
    elsif session[:main_controller] == 'events'
      link = link_to 'Перейти до секцій', clubs_path, class: 'go-to'
    end
    link
  end

  def home_url
    if session[:main_controller] == 'clubs'
      url = clubs_path
    elsif session[:main_controller] == 'events'
      url = events_path
    end
    url
  end

  def avatar_url(user)
    user.avatar_url.to_s == '' ? 'avatar.jpg' : user.avatar_url
  end

end
