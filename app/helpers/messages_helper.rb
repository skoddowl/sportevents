module MessagesHelper

  def show_date(message)
    date = message.created_at
    if date.to_date == Date.today
      date.strftime("%H:%M")
    elsif date.to_date == Date.yesterday
      date.strftime("Вчора, %H:%M")
    else
      date.strftime("%H:%M %m.%d")
    end
  end

end
