module EventsHelper

  def show_date(event)
    date = event.begins_at
    if date.to_date == Date.today
      date.strftime("%H:%M")
    elsif date.to_date == Date.tomorrow
      date.strftime("Завтра, %H:%M")
    else
      date.strftime("%H:%M %m.%d")
    end
  end

end
